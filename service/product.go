package service

import (
	"context"
	pb "temp/genproto/product"
	"temp/pkg/logger"
	"temp/storage"

	"github.com/jmoiron/sqlx"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ProductService struct{
	storage storage.IStorage
	logger logger.Logger
}

func NewProductService(db *sqlx.DB, log logger.Logger) *ProductService{
	return &ProductService{
		storage: storage.NewStoragePg(db),
		logger: log,
	}
}

func (p *ProductService) CreateProduct(ctx context.Context, req *pb.Product) (*pb.Product, error) {
	productReq, err := p.storage.Product().CreateProduct(req)
	if err != nil{
		p.logger.Error("Error with Product",logger.Any("error insert Product",err))
		return &pb.Product{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}
	return productReq, nil
}

func (p *ProductService) GetProduct(ctx context.Context, req *pb.ProductID) (*pb.Product, error) {
	productReq, err := p.storage.Product().GetProduct(req.Id)
	if err != nil{
		p.logger.Error("Error with Product",logger.Any("error insert Product",err))
		return &pb.Product{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}
	return productReq, nil
}

func (p *ProductService) UpdateProduct(ctx context.Context, req *pb.Product) (*pb.Product, error) {
	productReq, err := p.storage.Product().UpdateProduct(req)
	if err != nil{
		p.logger.Error("Error with Product",logger.Any("error insert Product",err))
		return &pb.Product{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}
	return productReq, nil
}

func (p *ProductService) DeleteProduct(ctx context.Context, req *pb.ProductID) (*pb.Product, error) {
	productReq, err := p.storage.Product().DeleteProduct(req.Id)
	if err != nil{
		p.logger.Error("Error with Product",logger.Any("error insert Product",err))
		return &pb.Product{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}
	return productReq, nil
}