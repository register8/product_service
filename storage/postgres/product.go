package postgres

import (
	"temp/genproto/product"
	"github.com/jmoiron/sqlx"
)

type productRepo struct{
	db *sqlx.DB
}

func NewProductRepo(db *sqlx.DB) *productRepo{
	return &productRepo{db: db}
}

func (p *productRepo) CreateProduct(req *product.Product) (*product.Product, error) {
	productReps := product.Product{}
	err := p.db.QueryRow(`insert into products(name, model) values($1, $2) 
	returning id, name, model`,req.Name, req.Model).Scan(&productReps.Id, &productReps.Name, &productReps.Model)
	if err != nil{
		return &product.Product{}, err
	}
	return &productReps, nil
}

func (p *productRepo) GetProduct(productID int64) (*product.Product, error) {
	productReps := product.Product{}
	err := p.db.QueryRow(`select id, name, model from products where id = $1`,productID).Scan(&productReps.Id, &productReps.Name, &productReps.Model)
	if err != nil{
		return &product.Product{}, err
	}
	return &productReps, nil
}

func (p *productRepo) UpdateProduct(req *product.Product) (*product.Product, error) {
	productResp := product.Product{}
	_, err := p.db.Exec(`update products SET name = $1, model = $2 where id = $3`,req.Name, req.Model, req.Id)
	if err != nil{
		return &product.Product{}, nil
	}
	return &productResp, nil
}

func (p *productRepo) DeleteProduct(productID int64) (*product.Product, error) {
	productReps := product.Product{}
	_, err := p.db.Query(`delete from products where id=$1`,productID)
	if err != nil{
		return &product.Product{}, err
	}
return &productReps, nil
}