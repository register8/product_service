package repo

import pb "temp/genproto/product"

type ProductStorageI interface{
	CreateProduct(*pb.Product) (*pb.Product, error)
	GetProduct(productID int64) (*pb.Product, error)
	UpdateProduct(*pb.Product) (*pb.Product, error)
	DeleteProduct(productID int64) (*pb.Product, error)
}